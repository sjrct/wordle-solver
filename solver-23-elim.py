import random
from pprint import pprint

WORD_LEN = 5
MAX_GUESSES = 6

def update_counts(word, counts):
  for i in range(0, len(word)):
    l = word[i]
    counts[i][l] = counts[i].get(l, 0) + 1

def load(dict_fn):
  words = []
  counts = [{}, {}, {}, {}, {}]

  with open(dict_fn) as f:
    for line in f.readlines():
      word = line.strip()
      if len(word) == WORD_LEN and word.islower() and word.isalpha():
        words.append(word)
        update_counts(word, counts)

  return (words, counts)

def word_value(word, counts):
  uniques = ''.join(set(word))
  total = 0
  for l in uniques:
    total += max([counts[i][l] for i in range(0, WORD_LEN) if word[i] == l])
  return total

def has_greens(greens, word):
  for (l, i) in greens:
    if word[i] != l:
      return False
  return True

def has_yellows(yellows, word):
  for l in yellows:
    if l not in word:
      return False
  return True

def has_not_yellow_poses(yellow_poses, word):
  for (l, i) in yellow_poses:
    if word[i] == l:
      return False
  return True

def has_not_grays(grays, word):
  for l in grays:
    if l in word:
      return False
  return True

def best_guess(greens, yellows, yellow_poses, grays, sort_words):
  for word in sort_words:
    if has_greens(greens, word) and has_yellows(yellows, word) and has_not_yellow_poses(yellow_poses, word) and has_not_grays(grays, word):
      return word
  raise Exception('word not in dictionary')

def eliminate_guess(greens, yellows, yellow_poses, grays, sort_words):
  already_got = [l for (l, _) in greens] + yellows + grays

  for word in sort_words:
    if has_not_grays(already_got, word):
      return word

  return best_guess(greens, yellows, yellow_poses, grays, sort_words)

def update_info(guess, target, greens, yellows, yellow_poses, grays):
  for i in range(0, WORD_LEN):
    l = guess[i]
    if l == target[i]:
      if (l, i) not in greens:
        greens.append( (l, i) )
    elif l in target:
      if l not in yellows:
        yellows.append(l)
      if (l, i) not in yellow_poses:
        yellow_poses.append( (l, i) )
    else:
      if l not in grays:
        grays.append(l)

def solve(target, sort_words):
  greens = []
  yellows = []
  yellow_poses = []
  grays = []

  for num in range(1, MAX_GUESSES + 1):
    if num <= 6:
      guess = eliminate_guess(greens, yellows, yellow_poses, grays, sort_words)
    else:
      guess = best_guess(greens, yellows, yellow_poses, grays, sort_words)
    print(f'{num}: {guess}')

    if guess == target:
      print('got it :)')
      return 1

    update_info(guess, target, greens, yellows, yellow_poses, grays)
    print("greens:", greens)
    print("yellows:", yellows)
    print("yellow_poses:", yellow_poses)
    print("grays:", grays)

  print('didnt get it :(')
  print('it was', target)
  return 0

if __name__ == '__main__':
  (words, counts) = load('words')
  sort_words = sorted(words, key=lambda w: -word_value(w, counts))

  print('word count =', len(words))
  for w in sort_words[:10] + sort_words[-10:]:
    print(w, word_value(w, counts))

  #with open('misses') as f:
  #  try_words = [l.strip() for l in f.readlines()]
  try_words = words

  successes = 0
  for target in try_words:
    successes += solve(target, sort_words)

  print(f'Got {successes}/{len(try_words)}')
