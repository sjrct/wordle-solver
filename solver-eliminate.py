import random
from pprint import pprint

WORD_LEN = 5
MAX_GUESSES = 6

def update_counts(word, counts):
  for i in range(0, len(word)):
    l = word[i]
    counts[i][l] = counts[i].get(l, 0) + 1

def load(dict_fn):
  words = []
  counts = [{}, {}, {}, {}, {}]

  with open(dict_fn) as f:
    for line in f.readlines():
      word = line.strip()
      if len(word) == WORD_LEN and word.islower() and word.isalpha():
        words.append(word)
        update_counts(word, counts)

  return (words, counts)

def word_value(word, counts):
  uniques = ''.join(set(word))
  total = 0
  for l in uniques:
    total += max([counts[i][l] for i in range(0, WORD_LEN) if word[i] == l])
  return total

def word_value_only(word, counts, only, pos):
  uniques = ''.join(set(word))
  total = 0
  for l in uniques:
    if l in only:
      total += counts[pos].get(l, 0)
  return total

def has_greens(greens, word):
  for (l, i) in greens:
    if word[i] != l:
      return False
  return True

def has_yellows(yellows, word):
  for l in yellows:
    if l not in word:
      return False
  return True

def has_not_yellow_poses(yellow_poses, word):
  for (l, i) in yellow_poses:
    if word[i] == l:
      return False
  return True

def has_not_grays(grays, word):
  for l in grays:
    if l in word:
      return False
  return True

def best_guess(greens, yellows, yellow_poses, grays, sort_words):
  for word in sort_words:
    if has_greens(greens, word) and has_yellows(yellows, word) and has_not_yellow_poses(yellow_poses, word) and has_not_grays(grays, word):
      return word
  raise Exception('word not in dictionary')

def potential_guesses(greens, yellows, yellow_poses, grays, sort_words):
  total = 0
  for word in sort_words:
    if has_greens(greens, word) and has_yellows(yellows, word) and has_not_yellow_poses(yellow_poses, word) and has_not_grays(grays, word):
      total += 1
  return total

def get_eliminate_pos(greens):
  nums = [i for (l, i) in greens]
  for i in range(0, WORD_LEN):
    if i not in nums:
      return i
  raise Exception('shouldnt be here')

def eliminate_guess(greens, grays, sort_words, counts):
  pos = get_eliminate_pos(greens)
  sort_words = sorted(sort_words, key=lambda w: word_value_only(w, counts, grays, pos))
  return sort_words[0]

def update_info(guess, target, greens, yellows, yellow_poses, grays):
  for i in range(0, WORD_LEN):
    l = guess[i]
    if l == target[i]:
      if (l, i) not in greens:
        greens.append( (l, i) )
    elif l in target:
      if l not in yellows:
        yellows.append(l)
      if (l, i) not in yellow_poses:
        yellow_poses.append( (l, i) )
    else:
      if l not in grays:
        grays.append(l)

def solve(target, sort_words, counts):
  greens = []
  yellows = []
  yellow_poses = []
  grays = []

  for num in range(1, MAX_GUESSES + 1):
    if len(greens) == 4 and num < 6 and potential_guesses(greens, yellows, yellow_poses, grays, sort_words) > 2:
      # special case for when there is a bunch of options left
      print('ELIMINATE')
      guess = eliminate_guess(greens, grays, sort_words, counts)
    else:
      guess = best_guess(greens, yellows, yellow_poses, grays, sort_words)
    print(f'{num}: {guess}')

    if guess == target:
      print('got it :)')
      return 1

    update_info(guess, target, greens, yellows, yellow_poses, grays)
    print("greens:", greens)
    print("yellows:", yellows)
    print("yellow_poses:", yellow_poses)
    print("grays:", grays)

  print('didnt get it :(')
  print('it was', target)
  return 0

if __name__ == '__main__':
  (words, counts) = load('words')
  sort_words = sorted(words, key=lambda w: -word_value(w, counts))

  print('word count =', len(words))
  for w in sort_words[:10] + sort_words[-10:]:
    print(w, word_value(w, counts))

  with open('misses') as f:
    try_words = [l.strip() for l in f.readlines()]

  successes = 0
  for target in try_words:
    successes += solve(target, sort_words, counts)

  print(f'Got {successes}/{len(try_words)}')
